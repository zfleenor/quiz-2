package com.example.zfleenor.quiz_2_zf;


import android.app.FragmentTransaction;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase db;

    Button create_button;
    Button insert_button;
    Button find_button;
    Button update_button;
    Button delete_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        create_button = (Button) findViewById(R.id.create_table_button);

        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatabase();
            }
        });

        insert_button = (Button) findViewById(R.id.insert_row_button);

        insert_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertDBRow();
            }
        });

        find_button = (Button) findViewById(R.id.find_person_button);

        find_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDB();
                db.query(studentTable);
            }
        });

        update_button = (Button) findViewById(R.id.update_row_button);

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDB();
                db.execSQL( "update studentTable");
            }
        });

        delete_button = (Button) findViewById(R.id.delete_row_button);

        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDB();
                db.execSQL( "delete studentTable");            }
        });

       }
}



    ///////////////////////////////////////////////////////////////////////////////
    private void openDatabase() {
        try{
        File storagePath=getApplication().getFilesDir();
        StringmyDbPath=storagePath+"/"+"myfriends"; txtMsg.setText("DBPath:"+myDbPath);

        db = SQLiteDatabase.openDatabase(myDbPath,null,
                SQLiteDatabase.CREATE_IF_NECESSARY);
         finish();
    }
        catch(SQLiteException e){
            finish();
        }
    }//openDatabase

/////////////////////////////////////////////////////////////////////////////////////
    private void insertDBRow() {
        db.execSQL( "insert into studentTable(RNumber, Name, Random, Phone) values ('R55566677', 'EEE FFF', '14', '523-4567);" );
    }
/////////////////////////////////////////////////////////////////////////////////////
    private void updateDB(){

    }
/////////////////////////////////////////////////////////////////////////////////////
    private void deleteDB() {

    }
/////////////////////////////////////////////////////////////////////////////////////
    private void searchDB() {

    }